## Провайдер Inn.Logist


Контактні особи: Олексій Бубін (тел.: +38935115165)

Дата:11.09.2023

| Номер сценарію | Опис | Статус | Артефакт | Розмір | Коментар |
|----------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------|---------|
| 1 | Реєстрація е-ТТН в стані PLANNED <br/> Перевізник:(3393607603, TEST 4)<br/> Замовник:(3393607600, TEST 1)<br/> Вантажовідправник:(3393607601, TEST 2)<br/>Вантажоодержувач:(3393607602, TEST 3) | успіх | | | |
| | ідентифікатор е-ТТН: ff69af57-ab83-4cbd-8573-9f32c5301138 | | лог-файл <br/>testing/uncefact/Inn.Logist/1/log.log | 8Кб | |
| | | | XML тіло документа е-ТТН: testing/uncefact/Inn.Logist/1/ECMR_planed_1e250bfb-87c7-41dc-930a-3d9e70ac12b3_dc6d5f3f-0f18-4f9a-acc5-f72f9712fe8f.xml | 13Кб | |
| | | | скріншот е-ТТН testing/uncefact/Inn.Logist/1/ECMR_1.pdf | 247Кб | |
| 2 | Реєстрація е-ТТН в стані PICKUP <br/>Перевізник:(3393607603, TEST 4) <br/>Замовник:(3393607600, TEST 1) <br/>Вантажовідправник:(3393607601, TEST 2)<br/>Вантажоодержувач:(3393607602, TEST 3) | успіх | | | |
| | ідентифікатор е-ТТН: 56575d08-a1f5-4b0e-bb98-ad98b4558d0d | | лог-файл<br/>testing/uncefact/Inn.Logist/2/log.log | 17Кб | |
| | | | XML тіло документа е-ТТН: <br/>testing/uncefact/Inn.Logist/2/ECMR_pickup_56575d08-a1f5-4b0e-bb98-ad98b4558d0d_4c3b4d07-d88a-4a24-8a36-23964655c66d.xml | 29Кб | |
| | | | скріншот е-ТТН testing/uncefact/Inn.Logist/2/ECMR_2.pdf | 247Кб | |
| 3 | Реєстрація е-ТТН в стані ARRIVAL <br/>Перевізник:(3393607603, TEST 4) <br/>Замовник:(3393607600, TEST 1) <br/>Вантажовідправник:(3393607601, TEST 2)<br/>Вантажоодержувач:(3393607602, TEST 3) | успіх | | | |
| | ідентифікатор е-ТТН: 4ad70590-e9d2-4ea1-a3ba-a9f6ff1e529d | | лог-файл <br/>testing/uncefact/Inn.Logist/3/log.log | 25Кб | |
| | | | XML тіло документа е-ТТН: <br/>testing/uncefact/Inn.Logist/3/ECMR_arrival_4ad70590-e9d2-4ea1-a3ba-a9f6ff1e529d_2defddc1-c3b8-4903-9338-b53f29cfc2e8.xml | 68Кб | |
| | | | скріншот е-ТТН testing/uncefact/Inn.Logist/3/ECMR_3.pdf | 247Кб | |
| 4 | Реєстрація в е-ТТН третіх сторін THIRD_PARTY <br/>Перевізник:(3393607603, TEST 4) <br/>Замовник:(3393607600, TEST 1) <br/>Вантажовідправник:(3393607601, TEST 2)<br/>Вантажоодержувач:(3393607602, TEST 3) <br/>Третя сторона :(3393607604, TEST 5) | успіх | | | |
| | ідентифікатор е-ТТН: 3f5a5424-108d-4fe4-b8f1-4bd64b663222 | | лог-файл <br/>testing/uncefact/Inn.Logist/4/log.log | 36Кб | |
| | | | XML тіло документа е-ТТН: <br/>testing/uncefact/Inn.Logist/4/ECMR_arrival_3f5a5424-108d-4fe4-b8f1-4bd64b663222_5774deea-5a0f-4206-844b-2c4f73351dbb.xml | 159Кб | |
| | | | скріншот е-ТТН<br/>testing/uncefact/Inn.Logist/4/ECMR_4.pdf | 247Кб | |
| 5 | Примусове завершення е-ТТН | | | | |
| 5.1 | Завершення перевезення з Актом примусового завершення е-ТТН <br/>Перевізник:(3393607603, TEST 4)<br/>Замовник:(9876543210, test FOP) | успіх | | | |
| | ідентифікатор е-ТТН: 44715ace-f9c0-4cb9-a422-25ff32d63212 | | лог-файл <br/>testing/uncefact/Inn.Logist/5/1/log.log | 29Кб | |
| | | | XML тіло документа е-ТТН (статус PICKUP)<br/>testing/uncefact/Inn.Logist/5/1/ECMR_pickup_44715ace-f9c0-4cb9-a422-25ff32d63212_c1627d8c-7410-4881-a829-2662fdec7a6d.xml | 29Кб | |
| | | | XML тіло Акта примусового завершення (стан SIGNED) <br/>testing/uncefact/Inn.Logist/5/1/ECMR_signed_55ecd740-6145-4470-83cc-e5de3b5edaf9_588b23ee-8df5-4cd5-b5da-684e0eb16d6f.xml | 15Кб | |
| | | | скріншот Акта примусового завершення <br/>testing/uncefact/Inn.Logist/5/1/5.1.pdf | 174Кб | |
| | | | скріншот е-ТТН <br/>testing/uncefact/Inn.Logist/5/1/ECMR_5.1.pdf | 247Кб | |
| 5.2 | Реєстрація Акта про відмову вантажити<br/>Вантажовідправник:(3393607601, TEST 2)<br/>Перевізник:(3393607603, TEST 4) | успіх | | | |
| | ідентифікатор е-ТТН: c1271211-9389-4b5b-a5a3-1aeb7c9d124e | | лог-файл <br/>testing/uncefact/Inn.Logist/5/2/log.log | 20Кб | |
| | | | XML тіло документа е-ТТН (статус PLANNED)<br/>testing/uncefact/Inn.Logist/5/2/ECMR_planned_c1271211-9389-4b5b-a5a3-1aeb7c9d124e_81aef4dd-976c-488a-9cee-ab9e542dda79.xml | 13Кб | |
| | | | XML тіло Акта примусового завершення (стан SIGNED) <br/>testing/uncefact/Inn.Logist/5/2/ECMR_signed_c5961024-eae2-4fef-bdc9-ac764233802f_4e52a440-6e40-4ab1-b3f5-7853299da1cf.xml | 16Кб | |
| | | | скріншот Акта про відмову вантажити <br/>testing/uncefact/Inn.Logist/5/2/5.2.pdf | 183Кб | |
| | | | скріншот е-ТТН <br/>testing/uncefact/Inn.Logist/5/2/ECMR_5.2.pdf | 247Кб | |
| 6 | Перевезення з перевантаженням в дорозі на проміжному складі | | | | |
| 6.1 | Реєстрація Акта розвантаження на проміжному складі<br/>Перевізник:(3393607603, TEST 4)<br/>Проміжний склад:(3393607604, TEST 5) | успіх | | | |
| | ідентифікатор е-ТТН: 95c5d6bf-0017-46ca-b210-8993889f21e2 | | лог-файл<br/>testing/uncefact/Inn.Logist/6/1/log.log | 32Кб | |
| | | | XML тіло документа е-ТТН (стан PICKUP)<br/>testing/uncefact/Inn.Logist/6/1/ECMR_pickup_95c5d6bf-0017-46ca-b210-8993889f21e2_6ee61856-053e-42d0-879f-71b8d6dd37f2.xml | 29Кб | |
| | | | XML тіло Акта розвантаження на проміжному складі (стан SIGNED)<br/>testing/uncefact/Inn.Logist/6/1/ECMR_signed_009424a6-b0b3-44a3-886f-201b29ca7423_e5f07273-98f2-4d2f-b064-ef77cb267db4.xml | 18Кб | |
| | | | скріншот Акта розвантаження на проміжному складі<br/>testing/uncefact/Inn.Logist/6/1/6.1.pdf | 188Кб | |
| | | | скріншот е-ТТН<br/>testing/uncefact/Inn.Logist/6/1/ECMR_6.1.pdf | 247Кб | |
| 6.2 | Реєстрація оновленої е-ТТН (в стані ONSTORAGE)<br/>Перевізник:(3393607603, TEST 4)<br/>Проміжний склад:(3393607604, TEST 5) | успіх | | | |
| | ідентифікатор е-ТТН: 95c5d6bf-0017-46ca-b210-8993889f21e2 | | лог-файл<br/>testing/uncefact/Inn.Logist/6/2/log.log | 10Кб | |
| | | | XML тіло документа е-ТТН (стан ONSTORAGE)<br/>testing/uncefact/Inn.Logist/6/2/ECMR_onstorage_95c5d6bf-0017-46ca-b210-8993889f21e2_03919b05-47e7-443c-90b8-0007db093f49.xml | 68Кб | |
| | | | скріншот е-ТТН<br/>testing/uncefact/Inn.Logist/6/2/ECMR_6.2.pdf | 247Кб | |
| 6.3 | Реєстрація Акта завантаження на проміжному складі<br/>Перевізник:(3393607603, TEST 4)<br/>Проміжний склад:(3393607604, TEST 5) | успіх | | | |
| | ідентифікатор е-ТТН: 95c5d6bf-0017-46ca-b210-8993889f21e2 | | лог-файл<br/>testing/uncefact/Inn.Logist/6/3/log.log | 12Кб | |
| | | | XML тіло Акта завантаження на проміжному складі(стан SIGNED)<br/>testing/uncefact/Inn.Logist/6/3/ECMR_signed_8e863964-364c-46bc-ac00-78d9d6980632_2cec64c1-b731-470f-9f91-eec524748855.xml | 18Кб | |
| | | | скріншот Акта завантаження на проміжному складі<br/>testing/uncefact/Inn.Logist/6/3/6.3.pdf | 188Кб | |
| | | | скріншот е-ТТН<br/>testing/uncefact/Inn.Logist/6/3/ECMR_6.3.pdf | 247Кб | |
| 6.4 | Реєстрація оновленої е-ТТН (в стані PICKUP)<br/>Перевізник:(3393607603, TEST 4)<br/>Проміжний склад:(3393607604, TEST 5) | успіх | | | |
| | ідентифікатор е-ТТН: 95c5d6bf-0017-46ca-b210-8993889f21e2 | | лог-файл<br/>testing/uncefact/Inn.Logist/6/4/log.log | 11Кб | |
| | | | XML тіло документа е-ТТН (стан PICKUP<br/>testing/uncefact/Inn.Logist/6/4/ECMR_pickup_95c5d6bf-0017-46ca-b210-8993889f21e2_d02e28ae-7d15-4fc1-87a4-8270b465bdd7.xml | 81Кб | |
| | | | скріншот е-ТТН<br/>testing/uncefact/Inn.Logist/6/4/ECMR_6.4.pdf | 247Кб | |
| 8 | Заміна пункту призначення | | | | |
| 8.1 | Заміна пункту призначення(Замовник:(9876543210, test FOP)(Вантажовідправник:(3393607601, TEST 2)(Перевізник:(3393607603, TEST 4) (Вантажоодержувач:(3393607602, TEST 3)(Пункт призначення:(Дніпро)) (Новий пункт призначення:(Львів)) | успіх |
| | ідентифікатор е-ТТН:d8c20411-6d43-4540-94dc-31680a41883b | | лог-файл<br/>testing/uncefact/Inn.Logist/8/1/log.log | 41Кб | |
| | | | XML тіло документа е-ТТН (статус PICKUP): testing/uncefact/Inn.Logist/8/1/ECMR_pickup_d8c20411-6d43-4540-94dc-31680a41883b_457ca884-ff7e-4116-9073-332960d304f2.xml | 29Кб | |
| | | | XML тіло Акта про заміну пункту призначення (стан SIGNED): testing/uncefact/Inn.Logist/8/1/ECMR_signed_771a7fe2-7e97-4ad6-b0be-20105d8e9f36_c52a94ad-2b23-4fd6-bafc-1d3d3420ec9c.xml | 62Кб | |
| | | | скріншот Акта про заміну пункту призначення: testing/uncefact/Inn.Logist/8/1/8.1.pdf | 180Кб | |
| 8.2 | Реєстрація оновленої е-ТТН зі зміненим пунктом призначення(Перевізник:(3393607603, TEST 4)(Вантажоодержувач:(3393607602, TEST 3) | успіх | | | |
| | ідентифікатор е-ТТН:d8c20411-6d43-4540-94dc-31680a41883b | | лог-файл<br/>testing/uncefact/Inn.Logist/8/2/log.log | 11Кб | |
| | | | XML тіло документа е-ТТН з новим пунктом призначення (статус PICKUP): testing/uncefact/Inn.Logist/8/2/ECMR_pickup_d8c20411-6d43-4540-94dc-31680a41883b_77384075-86e1-43e4-aebb-e80ca35e064a.xml | 51Кб | |
| | | | скріншот е-ТТН: testing/uncefact/Inn.Logist/8/2/ECMR_8.2.pdf | 246Кб | |
| 10 | Перепломбування в процесі перевезення | | | | |
| | ідентифікатор е-ТТН: f5e64ae8-da76-45a2-ab1e-c86094db90f3 | | | | |
| 10.1 | Реєстрація Акта перепломбування(Вантажовідправник:(3393607601, TEST 2)(Перевізник:(3393607603, TEST 4) | успіх | | | |
| | | | лог-файл<br/>testing/uncefact/Inn.Logist/10/1/log.log | 32Кб | |
| | | | XML тіло документа е-ТТН (статус PICKUP)<br/>testing/uncefact/Inn.Logist/10/1/ECMR_pickup_f5e64ae8-da76-45a2-ab1e-c86094db90f3_b9881fed-4e93-4fb6-919f-4d54f8022178.xml | 29Кб | |
| | | | XML тіло Акта перепломбування (стан SIGNED)<br/>testing/uncefact/Inn.Logist/10/1/ECMR_signed_e3ec1584-69d6-4210-9067-fc44445c9486_da6ca6c3-aada-4063-ae9e-5f62b50bf684_act.xml | 17Кб | |
| | | | скріншот Акта перепломбування<br/>testing/uncefact/Inn.Logist/10/1/10.1.pdf | 186Кб | |
| | | | скріншот е-ТТН<br/>testing/uncefact/Inn.Logist/10/1/ECMR_10.1.pdf | 246Кб | |
| 11 | Розбіжності при прийманні вантажу | | | | |
| 11.1 | Реєстрація Акта розбіжностей (Вантажовідправник:(3393607601, TEST 2)(Перевізник:(3393607603, TEST 4) (Вантажоодержувач:(3393607602, TEST 3) | успіх | | | |
| | ідентифікатор е-ТТН: | | лог-файл<br/>testing/uncefact/Inn.Logist/11/1/log.log | 32Кб | |
| | | | XML тіло документа е-ТТН (статус PICKUP)<br/>testing/uncefact/Inn.Logist/11/1/ECMR_pickup_3cbd30c8-762f-4d53-bbce-80be5e7a9101_ad461d1f-9db2-44b0-8f7f-0c65af1d16c1.xml | 29Кб | |
| | | | XML тіло Акта розбіжностей(стан SIGNED)<br/>testing/uncefact/Inn.Logist/11/1/ECMR_signed_e8708b96-15fe-424f-87b1-d858353d3624_bc6c7d2e-caf2-42e8-913a-fec8dfad0b6d_act.xml | 19Кб | |
| | | | скріншот е-ТТН<br/>testing/uncefact/Inn.Logist/11/1/ECMR_11.1.pdf | 246Кб | |
| | | | скріншот Акта розбіжностей<br/>testing/uncefact/Inn.Logist/11/1/11.1.pdf | 180Кб | |
| 12 | Неуспішне підписання Акта (тип Акта — на вибір провайдера) |  |  |  |  |
| 12.1 | Відміна Акта ініціатором (Вантажовідправник:(3393607601, TEST 2)(Перевізник:(3393607603, TEST 4) (Вантажоодержувач:(3393607602, TEST 3) | успіх |  |  |  |
| | ідентифікатор е-ТТН:743bd9bb-c1c1-48ef-b92c-aa8085fd7409 |  | лог-файл<br/>testing/uncefact/Inn.Logist/12/1/log.log | 32Кб |  |
|  |  |  | XML тіло документа е-ТТН<br/>testing/uncefact/Inn.Logist/12/1/ECMR_pickup_743bd9bb-c1c1-48ef-b92c-aa8085fd7409_5ac57ced-cac3-465f-af6d-0691094233a9.xml | 29Кб |  |
|  |  |  | XML тіло Акта<br/>testing/uncefact/Inn.Logist/12/1/ECMR_canceled_0f7937f8-48ef-4998-8bc0-6931cb14d6d6_fd263f0a-5563-4e1d-bd53-f8edf8797b88_act.xml | 7Кб |  |
|  |  |  | скріншот Акта<br/>testing/uncefact/Inn.Logist/12/1/12.1.pdf | 186Кб |  |
|  |  |  | скріншот е-ТТН<br/>testing/uncefact/Inn.Logist/12/1/ECMR_12.pdf | 246Кб |  |
| 12.2 | Відмова від підписання (Вантажовідправник:(3393607601, TEST 2)(Перевізник:(3393607603, TEST 4) (Вантажоодержувач:(3393607602, TEST 3) | успіх |  |  |  |
|  | ідентифікатор е-ТТН: 09632685-4a78-4aba-891c-45fd8fd04977 |  | лог-файл<br/>testing/uncefact/Inn.Logist/12/2/log.log | 40Кб |  |
|  |  |  | XML тіло документа е-ТТН<br/>testing/uncefact/Inn.Logist/12/2/ECMR_pickup_09632685-4a78-4aba-891c-45fd8fd04977_9786d199-9bdc-46c5-b39d-df4ca5020e01.xml | 29Кб |  |
|  |  |  | XML тіло Акта (статус PENDING_SIGNATURE)<br/>testing/uncefact/Inn.Logist/12/2/ECMR_pending_signature_bbf13bf8-bec6-46a7-8658-5a99bc501415_a54bfa91-da9c-4820-9fdd-bfe8be6e3d96_act.xml | 7Кб |  |
|  |  |  | XML тіло Акта  (статус CANCELED)<br/>testing/uncefact/Inn.Logist/12/2/ECMR_canceled_bbf13bf8-bec6-46a7-8658-5a99bc501415_80d1dfb5-1de5-41f9-bc12-b2b9fe80692b_act.xml | 17Кб |  |
|  |  |  | скріншот Акта (статус CANCELED)<br/>testing/uncefact/Inn.Logist/12/2/12.2.pdf | 186Кб |  |
|  |  |  | скріншот е-ТТН<br/>testing/uncefact/Inn.Logist/12/2/ECMR_12.2.pdf | 246Кб |  |
| 13 | Реєстрація супровідної документації Замовник:(9876543210, test FOP)<br/>Вантажовідправник:(3393607601, TEST 2)<br/>Перевізник:(3393607603, TEST 4)<br/>Вантажоодержувач:(3393607602, TEST 3) | успіх | | | |
| | ідентифікатор е-ТТН: a302ac97-ac3f-4ebd-b2e0-83138831ad25 | | лог-файл<br/>testing/uncefact/Inn.Logist/13/log.log | 20Кб | |
| | | | PDF сертифікату<br/>testing/uncefact/Inn.Logist/13/certificate.pdf | 69Кб | |
| | | | скріншот е-ТТН<br/>testing/uncefact/Inn.Logist/13/ECMR_13.pdf | 247Кб | |
| | | | XML тіло документа е-ТТН-<br/>testing/uncefact/Inn.Logist/13/ECMR_planed_a302ac97-ac3f-4ebd-b2e0-83138831ad25_49e04a77-3d89-4865-8f10-c371ddaad5c5.xml | 13Кб | |
| 14 | Робота конвеєра подій | | | | |
| 14.1 | Підписка на події в конвеєрі подій <br/>Перевізник:(9876543210, ТЕСТ Фізична Особа) | успіх | | | |
| | | | лог-файл<br/>testing/uncefact/Inn.Logist/14/1/log.log | 29Кб | |
| 14.2 | Отримання подій з конвеєру<br/>Перевізник:(9876543210, ТЕСТ Фізична Особа) | успіх | | | |
| | | | лог-файл<br/>testing/uncefact/Inn.Logist/14/2/log.log | 2Кб | |
| 15 | Перевірка інтероперабельності | | | | |
| 15.1 | Отримання документа е-ТТН з конвеєра подій<br/>Перевізник:(3393607603, TEST 4) <br/>Замовник:(3393607600, TEST 1) <br/>Вантажовідправник:(3393607601, TEST 2)<br/>Вантажоодержувач:(3393607602, TEST 3) <br/>Третя сторона :(9876543210, ТЕСТ Фізична Особа) | успіх | | | |
| | ідентифікатор е-ТТН: beee3afc-b142-4fb4-9852-4823560f34c1 | | лог-файл<br/>testing/uncefact/Inn.Logist/15/1/log.log | 16Кб | |
| | | | XML тіло документа е-ТТН: <br/>testing/uncefact/Inn.Logist/15/1/ECMR_planed_beee3afc-b142-4fb4-9852-4823560f34c1_2cc496de-674e-4da1-87fc-b87ee5a7883f.xml | 13Кб | |
| 15.2 | Отримання Акта з конвеєра подій<br/>Перевізник:(3393607603, TEST 4)<br/>Замовник:(9876543210, test FOP)<br/>Вантажовідправник:(3393607601, TEST 2)<br/>Вантажоодержувач:(3393607602, TEST 3) <br/> Третя сторона :(3393607600, TEST 1) | успіх | | | |
| | ідентифікатор е-ТТН: 59f04bf5-8ff1-4d50-87bf-a582ae914443 | | лог-файл<br/>testing/uncefact/Inn.Logist/15/2/log.log | 9Кб | |
| | | | XML тіло Акта: <br/> testing/uncefact/Inn.Logist/15/2/ECMR_pending_signature_act.xml | 7Кб | |